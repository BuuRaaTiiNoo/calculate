package com.epam;

import java.util.Arrays;
import java.util.List;

public class Main {

    private static Compute compute = new Compute();

    private static List<String> expressionList = Arrays.asList(
            "-2*2+1*3/3*1",
            "2/2",
            "2*2*3/3",
            "2/4",
            "-0/0",
            "1/0",
            "0/0"
    );

    public static void main(String[] args) {
        for (String exp : expressionList) {
            System.out.println(compute.solution(exp));
        }
    }
}
