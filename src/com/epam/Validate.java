package com.epam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {

    private LinkedList<String> linkedList;

    public LinkedList<String> stringByList(String expression) {
        String[] arr = expression.split("\\b(?<=\\d.?)");
        linkedList = new LinkedList<>(Arrays.asList(arr));
        return linkedList;
    }

    public boolean checkValidate(String expression) {
        Pattern p = Pattern.compile("^([\\-]?)(?:\\d+([\\*\\+\\/\\-]?))+\\d+$");
        Matcher m = p.matcher(expression);
        return m.matches();
    }
}
