package com.epam;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Compute {

    private Validate validate = new Validate();


    //Создаем лист операций
    private static final ArrayList<String> operations;

    static {
        operations = new ArrayList<>();
        operations.add("/");
        operations.add("*");
        operations.add("+");
        operations.add("-");
    }

    public String solution(String exp) {

        LinkedList<String> linkedList;

        //Проверяем удовлетворяют ли вводиые данные математическому выражению
        if (validate.checkValidate(exp)){
            linkedList = validate.stringByList(exp);

            for (String operation : operations) {
                ListIterator<String> listIterator = linkedList.listIterator();

                while (listIterator.hasNext()) {
                    if (listIterator.next().equals(operation)) {

                        //Удаляем из списка элемент с нечисловым значением(/,*,+,-)
                        listIterator.remove();

                        //Получаем операнды
                        double a = Double.parseDouble(linkedList.get(listIterator.previousIndex()));
                        double b = Double.parseDouble(linkedList.get(listIterator.nextIndex()));

                        //Заменяем предыдущий элемент листа на пустую строку
                        linkedList.set(listIterator.previousIndex(), "");

                        switch (operation) {
                            case "*":
                                linkedList.set(listIterator.nextIndex(), String.valueOf(a * b));
                                break;
                            case "/":
                                linkedList.set(listIterator.nextIndex(), String.valueOf(a / b));
                                break;
                            case "+":
                                linkedList.set(listIterator.nextIndex(), String.valueOf(a + b));
                                break;
                            case "-":
                                linkedList.set(listIterator.nextIndex(), String.valueOf(a - b));
                                break;
                        }
                    }
                }
                //Удаляем элементы с пустым значением из листа
                linkedList.removeIf(String::isEmpty);
            }
            return exp + " = " + linkedList.getFirst();
        }
        return "expression " + exp + " incorrect";
    }
}
